import os
from flask_script import Manager
from flask_wtf import CSRFProtect
from config import DevelopmentConfig

from app import create_app

app = create_app()
# Повтор в __init__.py
# app.config.from_object(DevelopmentConfig)
manager = Manager(app)

if __name__ == '__main__':
    manager.run()