class Config(object):
    DEBUG = False
    WTF_CSRF_ENABLED = True
    SECRET_KEY = ''
    WTF_CSRF_SECRET_KEY = ''
    PAGINATION_RANGE = 10


class ProductionConfig(Config):
    DEBUG = False


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
