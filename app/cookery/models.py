from datetime import time, timedelta
from decimal import Decimal

from app.database import db
from pony.orm import PrimaryKey, Required, Optional, Set


class Recipe(db.Entity):
    """Рецепт"""
    id = PrimaryKey(int, auto=True)
    name = Required(str, 200, index=True)  # Название блюда
    instruction = Required(str)  # Инструкция
    notes = Optional(str)  # Заметки
    servings = Required(int)  # Количество порций
    time = Required(timedelta)  # Время приготовления
    image = Optional(str)  # Изображение блюда
    ingredients = Set('Ingredient', cascade_delete=True)
    images = Set('Image', cascade_delete=True)
    tags = Set('Tag')
    users = Set('User')


class Measure(db.Entity):
    """Единица измерения"""
    id = PrimaryKey(int, auto=True)
    name = Required(str, 200)  # Название единицы измерения
    ingredients = Set('Ingredient')


class Product(db.Entity):
    """Продукт"""
    id = PrimaryKey(int, auto=True)
    name = Required(str, 200)  # Название продукта
    price = Required(Decimal)  # Примерная цена
    index = Required(int)
    ingredients = Set('Ingredient')


class Ingredient(db.Entity):
    """Ингредиент"""
    id = PrimaryKey(int, auto=True)
    product = Required(Product)
    quantity = Required(float)  # Количество
    recipe = Required(Recipe)
    measure = Required(Measure)


class Image(db.Entity):
    """Изображение рецепта"""
    id = PrimaryKey(int, auto=True)
    recipe = Required(Recipe)
    url = Required(str)  # URL изображения


class Tag(db.Entity):
    """Теги рецепта"""
    id = PrimaryKey(int, auto=True)
    name = Required(str, 200)  # имя тега
    recipes = Set(Recipe)
    category_tag = Required('CategoryTag')


class CategoryTag(db.Entity):
    """Категории тегов"""
    id = PrimaryKey(int, auto=True)
    name = Optional(str, 200)  # Название категории тега
    tags = Set(Tag)


class User(db.Entity):
    """Пользователи"""
    id = PrimaryKey(int, auto=True)
    favorites = Set(Recipe)  # Закладки
    username = Optional(str, 60)  # Имя пользователя
    telegram_id = Optional(int)  # id пользователя в telegram


# db.bind('sqlite', 'cookbook.db', create_db=True)
db.bind(provider='postgres', user='cookbook', password='pass', host='localhost', database='cookbook')
db.generate_mapping(create_tables=True)
