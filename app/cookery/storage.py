from pony.orm import db_session, select

from .models import Product, Ingredient


@db_session
def get_recipes_by_keywords(keywords, page, request):
    response = {
        'results': [],
        'next': '',
        'prev': '' if page == 1 else request.url.replace('page=' + str(page), 'page=' + str(page - 1)),
    }

    # set_sql_debug(True)
    # SELECT ингредиентов
    products = select(product for product in Product if product.name.lower() in keywords)
    # SELECT для рецептов, учитывая ингридиенты, выдаём страницами по 10 рецептов на каждую
    select_recipes = select((ingredient.recipe.id, ingredient.recipe.name)
                            for ingredient in Ingredient if ingredient.product in products)

    select_current_recipes = select_recipes.page(page, 10)

    if select_recipes.page(page + 1, 10):
        response['next'] = request.url.replace('page=' + str(page), 'page=' + str(page + 1))

    # Добавляем полученные рецепты в словарь для ответа
    for recipe_id, recipe_name in select_current_recipes:
        response['results'].append({'id': recipe_id, 'name': recipe_name})

    return response
