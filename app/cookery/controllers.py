from flask import Blueprint, request, render_template, url_for, redirect, jsonify

from pony.orm import db_session, commit

from .forms import RecipeForm
from .models import Recipe, Tag, CategoryTag, Ingredient, Product, Measure, User
from .serializers import recipe_to_dict, user_to_dict
from .storage import get_recipes_by_keywords


module = Blueprint('cookery', __name__, url_prefix='/cookery')


@module.route('/recipes/')
def recipes():
    """Для получения рецептов, можно передать ключевые слова в json"""
    response = {}

    # Получаем номер запрашиваемой страницы, если его нет, значит - первая
    pagenum = int(request.args.get('page')) if str(request.args.get('page')).isdigit() else 1
    # Получаем слова для поиска
    keywords = request.args.get('q').split(' ') if request.args.get('q') else None

    if keywords:
        response = get_recipes_by_keywords(keywords, pagenum, request)
        return jsonify(response)

    return jsonify(response)


@module.route('/recipes/<int:recipe_id>/')
@db_session
def get_recipe(recipe_id):
    """Получение рецепта по id"""

    telegram_id = int(request.args.get('telegram_id')) if str(request.args.get('telegram_id')).isdigit() else None

    user = User.get(telegram_id=telegram_id)

    response = {}
    recipe = Recipe.get(id=recipe_id)

    if recipe:
        # Отдаём только нужную информацию по рецепту
        response = recipe_to_dict(recipe, user)
    return jsonify(response)


@module.route('/users/<int:user_id>/favorites/', methods=['GET', 'POST'])
@db_session
def user_favorites(user_id):
    """Получение рецептов в закладках пользователя и добавление в закладки"""
    # Если GET, значит запрашиваем рецепты в закладках
    if request.method == 'GET':
        # Получаем номер запрашиваемой страницы
        page = int(request.args.get('page')) if str(request.args.get('page')).isdigit() else 1

        response = {
            'results': [],
            'next': '',
            # Если текущая страница - первая, значит предыдущей быть не может
            'prev': '' if page == 1 else request.url.replace('page=' + str(page), 'page=' + str(page - 1)),
        }
        user = User.get(id=user_id)

        if user:
            select_recipes = user.favorites
            # SELECT рецептов, которые будут выданы пользователю
            select_current_recipes = select_recipes.page(page, 10)

            # Проверка, есть ли рецепты на следующей страницы, если есть, то формируем url для next
            if select_recipes.page(page + 1, 10):
                response['next'] = request.url.replace('page=' + str(page), 'page=' + str(page + 1))

            for recipe in select_current_recipes:
                response['results'].append({'id': recipe.id, 'name': recipe.name})

        return jsonify(response)
    # Добавляет рецепты в закладки
    elif request.method == 'POST':
        user = User.get(id=request.form.get('user_id'))
        user.favorites += Recipe.get(id=request.form.get('recipe_id'))

        return '', 200


@module.route('/users/<int:user_id>/favorites/<int:recipe_id>/', methods=['DELETE'])
@db_session
def user_favorites_remove(user_id, recipe_id):
    """Удаляет рецепт из закладок"""
    user = User.get(id=user_id)
    recipe = Recipe.get(id=recipe_id)
    user.favorites.remove(recipe)

    return '', 204


@module.route('/users/<int:user_id>')
@db_session
def get_user(user_id):
    """Получение данных о пользователе"""

    response = {}

    user = User.get(id=user_id)

    if user:
        response = user_to_dict(user)

    return jsonify(response)


@module.route('/users/')
@db_session
def get_users():
    """Получение списка пользователей"""
    telegram_id = int(request.args.get('telegram_id')) if request.args.get('telegram_id').isdigit() else None

    response = {}
    # Если указан telegram_id, значит поиск пользователя по telegram_id
    if telegram_id:
        user = User.get(telegram_id=telegram_id)

        # Если нет такого пользователя - добавляем
        if not user:
            user = User(username='', telegram_id=telegram_id)

        # Запись в БД
        commit()

        response = user_to_dict(user)

    return jsonify(response)


@module.route('/', methods=['GET', 'POST'])
@db_session
def recipe_form():
    """Отображение и добавление новых рецептов"""
    if request.method == 'GET':
        form = RecipeForm()

        return render_template('recipe/form.html', form=form)
    elif request.method == 'POST':
        form = RecipeForm(request.form)

        if form.validate_on_submit():

            recipe = Recipe.get(name=form.data.get('name'))
            if not recipe:
                recipe_tags = []
                recipe_ingredients = []

                # Добавляем новый рецепт
                recipe = Recipe(name=form.data.get('name'), instruction=form.data.get('instruction'),
                                notes=form.data.get('notes'), servings=form.data.get('servings'),
                                time=form.data.get('time'))

                # Проходим по тегам и категориям, если чего-то нет, то добавляем
                for recipe_tag in form.data['tags']:
                    category = CategoryTag.get(name=recipe_tag.get('tag_category'))
                    tag = Tag.get(name=recipe_tag.get('tag_name'))

                    if not category:
                        category = CategoryTag(name=recipe_tag.get('tag_category'))

                    if not tag:
                        tag = Tag(name=recipe_tag.get('tag_name'), category_tag=category)
                    recipe_tags.append(tag)

                # Проходим по ингридиентам, если нет продукта или ед.изм., то добавляем
                for recipe_ingredient in form.data.get('ingredients'):
                    product = Product.get(name=recipe_ingredient.get('product'))
                    measure = Measure.get(name=recipe_ingredient.get('measure'))

                    if not product:
                        product = Product(name=recipe_ingredient.get('product'), price='1')

                    if not measure:
                        measure = Measure(name=recipe_ingredient.get('measure'))

                    ingredient = Ingredient(product=product, measure=measure,
                                            quantity=recipe_ingredient.get('quantity'),
                                            recipe=recipe)
                    recipe_ingredients.append(ingredient)

                # Связываем индридиенты и теги с рецептом
                recipe.ingredients = recipe_ingredients
                recipe.tags = recipe_tags

            return redirect(url_for('cookery.recipe_form'))
        return 'false'
