from flask import url_for, request


def recipe_to_dict(obj_recipe, obj_user=None):
    recipe = {
        'id': obj_recipe.id,
        'name': obj_recipe.name,
        'instruction': obj_recipe.instruction,
        'notes': obj_recipe.notes,
        'servings': obj_recipe.servings,
        'time': str(obj_recipe.time),
        'ingredients': [{
            'product': ingredient.product.name,
            'quantity': ingredient.quantity,
            'measure': ingredient.measure.name
        } for ingredient in obj_recipe.ingredients],
        # Если рецепт в закладках у пользователя - будет True, если нет - False,
        # если нет пользоателя, или не указан - None
        'favorite':  (True if obj_recipe in obj_user.favorites else False) if obj_user else None
    }
    return recipe


def user_to_dict(obj_user):
    user = {
        'id': obj_user.id,
        # 'username': obj_user.username,
        'telegram_id': obj_user.telegram_id,
        'favorites': request.url_root + url_for('cookery.user_favorites', user_id=obj_user.id)[1:],
    }

    return user
