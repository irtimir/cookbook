from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, IntegerField, FormField, FieldList
from wtforms.validators import InputRequired


class IngredietForm(FlaskForm):
    product = StringField('Продукт', validators=[InputRequired()])
    quantity = StringField('Количество', validators=[InputRequired()])
    measure = StringField('ед. изм.', validators=[InputRequired()])


class TagField(FlaskForm):
    tag_name = StringField('Название тега', validators=[InputRequired()])
    tag_category = StringField('Категория тега', validators=[InputRequired()])


class RecipeForm(FlaskForm):
    name = StringField('Название рецепта', validators=[InputRequired(), ])
    instruction = TextAreaField('Инструкция', validators=[InputRequired(), ])
    notes = TextAreaField('Заметки')
    servings = IntegerField('Количество порций', validators=[InputRequired(), ])
    time = StringField('Время приготовления', validators=[InputRequired()], )
    ingredients = FieldList(FormField(IngredietForm, 'Ингредиенты'), min_entries=1)
    tags = FieldList(FormField(TagField, 'Теги'), min_entries=1)
