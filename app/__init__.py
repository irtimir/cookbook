from flask import Flask
from flask_wtf import CSRFProtect
from config import DevelopmentConfig
from app.cookery.models import *


def create_app():
    app = Flask(__name__)

    app.config.from_object(DevelopmentConfig)

    csrf = CSRFProtect(app)

    import app.cookery.controllers as cookery

    # Отключаем проверку csrf на функциях, где используется POST и DELETE
    csrf.exempt(cookery.user_favorites)
    csrf.exempt(cookery.user_favorites_remove)

    app.register_blueprint(cookery.module)

    return app
